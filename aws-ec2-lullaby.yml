---
AWSTemplateFormatVersion: '2010-09-09'
Resources:
    Ec2LullabyLambdaFunction:
        Type: AWS::Lambda::Function
        Properties:
            Handler: index.lambda_handler
            FunctionName: Ec2Lullaby
            Description: Stops EC2 instances labeled as developer instances at a certain time to save on ops costs.
            Role: !GetAtt Ec2LullabyRole.Arn
            Code:
                ZipFile:
                    !Sub |
                        import boto3
                        import datetime
                        from botocore.exceptions import ClientError
                        ec2 = boto3.client("ec2")
                        stop_time_tag_key = "stoptime"
                        now = datetime.datetime.now()
                        print("Reference time:", now)
                        def instance_pages(Filters=None, DryRun=False):
                            response = ec2.describe_instances(Filters=Filters, DryRun=DryRun)
                            NextToken = response["NextToken"] if "NextToken" in response else None
                            yield response["Reservations"]
                            while NextToken:
                                response = ec2.describe_instances(Filters=Filters, DryRun=DryRun, NextToken=NextToken)
                                NextToken = response["NextToken"] if "NextToken" in response else None
                                yield response["Reservations"]
                        def time_in_range(start, end, target):
                            if start <= end:
                                return start <= target <= end
                            else:
                                return start <= target or target <= end
                        def time_to_shutdown(instance):
                            tag_list = instance["Tags"]
                            for tag in tag_list:
                                if tag["Key"] == stop_time_tag_key:
                                    stop_time_value = tag["Value"]
                            try:
                                stop_time = datetime.datetime.strptime(stop_time_value, "%H:%M")
                                stop_time_today = now.replace(hour=stop_time.hour, minute=stop_time.minute)
                            except Exception as e:
                                print("Error parsing {}".format(stop_time_tag_key), e)
                            try:
                                stop_time_earlier = stop_time_today - datetime.timedelta(minutes=15)
                                stop_time_later = stop_time_today + datetime.timedelta(minutes=15)
                            except Exception as e:
                                print(e)
                            return time_in_range(stop_time_earlier, stop_time_later, now)
                        def get_instance_name(instance):
                            tag_list = instance["Tags"]
                            for tag in tag_list:
                                if tag["Key"] == "Name":
                                    return tag["Value"]
                        dev_filter = [
                            {
                                "Name": "instance-state-name",
                                "Values" : [
                                    "running",
                                ],
                            },
                            {
                                "Name": "tag:environment",
                                "Values": [
                                    "dev",
                                    "development",
                                ],
                            },
                            {
                                "Name": "tag-key",
                                "Values": [
                                    stop_time_tag_key,
                                ]
                            }
                        ]
                        def main(DryRun=True):
                            try:
                                instances = []
                                for reservations in instance_pages(Filters=dev_filter, DryRun=DryRun):
                                    for reservation in reservations:
                                        instances.extend(reservation["Instances"])
                            except Exception as e:
                                print("Error describing instances", e)
                            for instance in instances:
                                if time_to_shutdown(instance):
                                    try:
                                        print("shutting down instance:", instance["InstanceId"], get_instance_name(instance))
                                        response = ec2.stop_instances(InstanceIds=[instance["InstanceId"]], DryRun=DryRun)
                                    except Exception as e:
                                        print("Error shutting down instance:", e)
                        def lambda_handler(event, context):
                            main(DryRun=False)
            Runtime: python3.6
    Ec2LullabyCloudWatchEvent:
        Type: AWS::Events::Rule
        Properties:
            Description: Triggers Lambda function which stops developer EC2 instances.
            ScheduleExpression: "cron(0 * * * ? *)"
            State: ENABLED
            Targets:
            - Arn: !GetAtt Ec2LullabyLambdaFunction.Arn
              Id: TargetFunction
    Ec2LullabyLambdaEventSource:
        Type: AWS::Lambda::Permission
        Properties:
            Action: lambda:InvokeFunction
            FunctionName: !GetAtt Ec2LullabyLambdaFunction.Arn
            Principal: events.amazonaws.com
            SourceArn: !GetAtt Ec2LullabyCloudWatchEvent.Arn
    Ec2LullabyRole:
        Type: AWS::IAM::Role
        Properties:
            AssumeRolePolicyDocument:
                Version: '2012-10-17'
                Statement:
                - Effect: Allow
                  Principal:
                    Service: 
                    - lambda.amazonaws.com
                  Action:
                  - sts:AssumeRole
            Path: "/ec2lullaby/"
            RoleName: Ec2LullabyRole
    Ec2LullabyPolicy:
        Type: AWS::IAM::Policy
        Properties:
            PolicyName: Ec2LullabyPolicyForEc2AndLogsAccess
            PolicyDocument:
                Version: '2012-10-17'
                Statement:
                - Effect: Allow
                  Action:
                  - "ec2:Describe*"
                  - "ec2:*Instances"
                  - "ec2:ModifyHosts"
                  - "ec2:GetConsoleOutput"
                  - "logs:*"
                  Resource: "*"
            Roles:
            - !Ref Ec2LullabyRole
