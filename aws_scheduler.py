import boto3
import datetime
from botocore.exceptions import ClientError

ec2 = boto3.client("ec2")

stop_time_tag_key = "stoptime"

# Get now here because we want to check against
# the invokation time of this function and don't
# want it to drift.
now = datetime.datetime.now()
print("Reference time:", now)

def instance_pages(Filters=None, DryRun=False):
    """
    Handles pagination of boto3 EC2 client as a generator.
    Use it like this:
    instances = []
    for page in instance_pages():
        instances.extend(page)
    """
    response = ec2.describe_instances(Filters=Filters, DryRun=DryRun)
    NextToken = response["NextToken"] if "NextToken" in response else None
    yield response["Reservations"]
    while NextToken:
        response = ec2.describe_instances(Filters=Filters, DryRun=DryRun, NextToken=NextToken)
        NextToken = response["NextToken"] if "NextToken" in response else None
        yield response["Reservations"]
        
# Borrowed from: https://stackoverflow.com/questions/10747974/how-to-check-if-the-current-time-is-in-range-in-python
def time_in_range(start, end, target):
    """Return true if target is in the range [start, end]"""
    if start <= end:
        return start <= target <= end
    else:
        return start <= target or target <= end

def time_to_shutdown(instance):
    # Get tag for stoptime
    tag_list = instance["Tags"]
    for tag in tag_list:
        if tag["Key"] == stop_time_tag_key:
            stop_time_value = tag["Value"]

    try:
        # Do it this way so we can
        # 1) Parse the time and check for errors
        # 2) Get the current date from now since parsing time alone
        #       gives 1-1-1900
        stop_time = datetime.datetime.strptime(stop_time_value, "%H:%M")
        stop_time_today = now.replace(hour=stop_time.hour, minute=stop_time.minute)
    except Exception as e:
        print("Error parsing {}".format(stop_time_tag_key), e)
        
    try:
        # Create a window for shutdown.  We want to allow booting up
        # in times after this time, as well, and not just compare against
        # times that are "after" the stop time.
        stop_time_earlier = stop_time_today - datetime.timedelta(minutes=15)
        stop_time_later = stop_time_today + datetime.timedelta(minutes=15)
    except Exception as e:
        print(e)
        
    return time_in_range(stop_time_earlier, stop_time_later, now)
    
def get_instance_name(instance):
    tag_list = instance["Tags"]
    for tag in tag_list:
        if tag["Key"] == "Name":
            return tag["Value"]

dev_filter = [
    {
        "Name": "instance-state-name",
        "Values" : [
            "running",
        ],
    },
    {
        "Name": "tag:environment",
        "Values": [
            "dev",
            "development",
        ],
    },
    {
        "Name": "tag-key",
        "Values": [
            stop_time_tag_key,
        ]
    }
]
def main(DryRun=True):
    try:
        instances = []
        for reservations in instance_pages(Filters=dev_filter, DryRun=DryRun):
            for reservation in reservations:
                instances.extend(reservation["Instances"])
    except Exception as e:
        print("Error describing instances", e)

    for instance in instances:
        if time_to_shutdown(instance):
            try:
                print("shutting down instance:", instance["InstanceId"], get_instance_name(instance))
                response = ec2.stop_instances(InstanceIds=[instance["InstanceId"]], DryRun=DryRun)
            except Exception as e:
                print("Error shutting down instance:", e)

def lambda_handler(event, context):
    main(DryRun=False)
